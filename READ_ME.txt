Python implementation to break 
1)One-way property 
2)Collision-Free property
using Brute force for the first n hex bits. I have taken n=5 (20 binary bits) of hash.
Here, I investigate the difference between hash function’s two properties: one-way property versus collision-free property. 
I have used the brute-force method to see how long it takes to break each of these properties.